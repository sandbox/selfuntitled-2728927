(function ($) {
    Drupal.behaviors.commerceSemantices3 = {
        attach: function (context, settings) {
            commerceSemantics3GetPhysicalFields(context);
        }
    };
    function commerceSemantics3BindButton(commerceSemantics3Attributes, context) {
        $('.commerce_semantics3_lookup', context).once('commerceSemantics3Lookup', function () {
            var commerceSemantics3AttributesJson = JSON.stringify(commerceSemantics3Attributes);
            $(this).click(function () {
                $(this).append("<div class='ajax-progress'><div class='throbber'></div>Querying Semantics3</div>")
                var data = {}, status = '';
                $.ajax({
                    url: '/admin/config/services/commerce_semantics3/ajax/query',
                    data: commerceSemantics3AttributesJson,
                    success : function (response, status, jqXHR) {
                        semantics3CommerceUpdate(response, status);
                    },
                    error: function (response, status, error) {
                        console.dirxml(response);
                        console.dir(response);
                        console.log(response);
                        $('.ajax-progress').remove();
                        switch (status) {
                            case "notmodified":
                                alert('Ajax error response not modified');
                                break;
                            case "error":
                                alert('Ajax error');
                                break;
                            case "timeout":
                                alert('Ajax timeout');
                                break;
                            case "parsererror":
                                alert('Ajax parse error');
                                break;
                        }
                    },
                    type: 'POST',
                    dataType: 'json'
                });
            });
        });
    }
    function commerceSemantics3GetPhysicalFields(context) {
        var commerceSemantics3Attributes = {
            sku: $("input[id*='sku']").val(),
            //weight:$('.field-type-physical-weight :input').val(),
            weightUnit: $('.field-type-physical-weight :selected').val(),
            dimensionsUnit: $('.physical-dimensions-unit-form-item :selected').val()
        };
        //console.dir(commerceSemantics3Attributes);
        commerceSemantics3BindButton(commerceSemantics3Attributes, context);
    }
    function semantics3CommerceUpdate(data, status, xhr) {
        var results = $.parseJSON(data);
        console.log(data);
        if ( results.error === false) {
            $('.field-type-physical-weight :input').val(results.weight);
            $($('.physical-dimension-form-item :input')[0]).val(results.length);
            $($('.physical-dimension-form-item :input')[1]).val(results.width);
            $($('.physical-dimension-form-item :input')[2]).val(results.height);
            $('.ajax-progress').remove();
        } else {
            console.log(results.error);
            alert(results.error);
            $('.ajax-progress').remove();
        }
    }
})(jQuery);