<?php
/**
 * Created by PhpStorm.
 * User: Me
 * Date: 5/8/2016
 * Time: 12:42 AM
 */

/**
 * @file
 * Administration pages for commerce_semantics3 module
 */

/**
 * Administration form for commerce_semantics3 module
 */
function commerce_semantics3_admin_settings($form, &$form_state) {

    $endpoints = array(
        //TODO: Allow user input of endpoints, to handle changes at Semantics3
        'https://api.semantics3.com/test/v1/products' => t('Testing/Development'),
        'https://api.semantics3.com/v1/' => t('Production'),
    );

    $form['commerce_semantics3_endpoint'] = array(
        '#type' => 'radios',
        '#title' => t('Endpoint'),
        '#description' => t('Select if requests should use the testing/development endpoint or production.  Testing/Development is limited to 100 calls per day.'),
        '#options' => $endpoints,
        '#default_value' => variable_get('commerce_semantics3_endpoint'),
    );

    $form['commerce_semantics3_key'] = array(
        '#type' => 'textfield',
        '#title' => t('API Key'),
        '#description' => t('Enter the API Key from your Semantics3 Dashboard. If you don\'t have one, you can create a free account at <a href="https://www.semantics3.com" target="_blank">Semantics3.com</a>' ),
        '#default_value' => isset($form_state['values']['commerce_semantics3_key']) ? $form_state['values']['commerce_semantics3_key'] : variable_get('commerce_semantics3_key'),
        '#required' => TRUE,
    );

    $form['commerce_semantics3_secret'] = array(
        '#type' => 'textfield',
        '#title' => t('Secret'),
        '#description' => t('Enter the Secret from your Semantics3 Dashboard.'),
        '#default_value' => isset($form_state['values']['commerce_semantics3_secret']) ? $form_state['values']['commerce_semantics3_secret'] : variable_get('commerce_semantics3_secret'),
        '#states' => array(
            'invisible' => array(
                ':input[name="commerce_semantics3_endpoint"]' => array('value' => 'https://api.semantics3.com/test/v1/'),
            ),
            'visible' => array(
                ':input[name="commerce_semantics3_endpoint"]' => array('value' => 'https://api.semantics3.com/v1/'),
            )
        ),
        '#required' => TRUE,
    );

    $form['commerce_semantics3_watchdog'] = array(
        '#title' => t('Log all requests to Watchdog'),
        '#type' => 'checkbox',
        '#default_value' => isset($form_state['values']['commerce_semantics3_watchdog']) ? $form_state['values']['commerce_semantics3_watchdog'] : variable_get('commerce_semantics3_watchdog', TRUE),
    );

    $form['#access'] = array('Administer Commerce Semantics3');
    $form['#validate'][] = 'commerce_semantics3_admin_settings_validate';

    return system_settings_form($form);
}

/**
 * Validation callback
 */
function commerce_semantics3_admin_settings_validate($form, &$form_state) {
    $values = $form_state['values'];

    if ( !ctype_alnum($values['commerce_semantics3_key'])  ) {
      form_set_error('commerce_semantics3_key', t('Invalid Semantics 3 Key - Should be alphanumeric'));
    }

    if ( !ctype_alnum($values['commerce_semantics3_secret'])  ) {
        form_set_error('commerce_semantics3_secret', t('Invalid Semantics 3 Secret - Should not include numbers'));
    }

}

